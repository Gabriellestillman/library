public class Periodical extends LibraryItem {
    String subject;
    

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Periodical(String itemName, boolean isAvaliable, Type type, String subject) {
        super(itemName, isAvaliable, type);
        this.subject = subject;
    }

    
}