public class CD extends LibraryItem {
    String artist;

    public CD(String itemName, boolean isAvaliable, Type type, String artist) {
        super(itemName, isAvaliable, type);
        this.artist = artist;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
    

}