enum Genre{
    NON_FICTION(1),
    COMEDY(2),
    ROMANCE(3),
    MYSTERY(4),
    ACTION(5),
    CHILDREN(6);
    int genreCode;
private Genre(int genreCode){
    this.genreCode=genreCode;
}
}