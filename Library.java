import java.util.*;


public class Library{
    private ArrayList<LibraryItem> inventory= new ArrayList<LibraryItem>();
    private ArrayList<LibraryItem> checkedOut= new ArrayList<LibraryItem>();
    
    

    public Library(ArrayList<LibraryItem> inventory) {
        this.inventory = inventory;
    }
    
    void addItem(LibraryItem i){
        this.inventory.add(i);
    }
    void removeItem(LibraryItem i){
        this.inventory.remove(i);
    }

    public ArrayList<LibraryItem> getInventory() {
        return inventory;
    }

    public void setInventory(ArrayList<LibraryItem> inventory) {
        this.inventory = inventory;
    }

    
    public String toString() {
        if(inventory.size()==0) return "Library is empty.";
        StringBuffer retString=new StringBuffer();
        for(LibraryItem i: inventory){
            retString.append(i.getItemName() + "\n");
        }
        return retString.toString();
    }
    
}