import java.util.*;
public class Account {
    private String username;
    private ArrayList<LibraryItem> checkedOutItems;
    final static int MAX_CHECKED_OUT=5;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ArrayList<LibraryItem> getCheckedOutItems() {
        return checkedOutItems;
    }

    public void setCheckedOutItems(ArrayList<LibraryItem> checkedOutItems) {
        this.checkedOutItems = checkedOutItems;
    }

    public static int getMaxCheckedOut() {
        return MAX_CHECKED_OUT;
    }

    public Account(String username, ArrayList<LibraryItem> checkedOutItems) {
        this.username = username;
        this.checkedOutItems = checkedOutItems;
    }
    
    public boolean checkout(ArrayList<LibraryItem> items){
        boolean allItemsAvaliable=true; 
        if(checkedOutItems.size()+items.size() > 5){
            System.out.println("Sorry, you have too many things checked out. Please return an item to check more out. \n");
            return false;
        }
        for(LibraryItem i: items){
            if(checkedOutItems.contains(i)){
                break;
            }
            if(!i.isAvaliable() && !checkedOutItems.contains(i)){
                System.out.println("Sorry, " +this.username+"," +i.getItemName()+ "is not avaliable right now. \n");
                System.out.println("Continuing to check out your other items..\n");
                allItemsAvaliable=false;
            }
            else{
                System.out.println("Checking out " +i.getItemName()+ " now, "+this.username+". \n");
                this.checkedOutItems.add(i);
                i.setAvaliable(false);


            }
             
        }
        return allItemsAvaliable; //signals that all requested items have been successfully checked out or not
    }
    public boolean returnItems(ArrayList<LibraryItem> returners){
        for(LibraryItem i: returners){
            i.isAvaliable=true;
            System.out.println("Thank you for returning "+i.getItemName()+", "+this.username+"\n");
            this.checkedOutItems.remove(i);
        }
        return true;
    }



}