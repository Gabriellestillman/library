public  class  LibraryItem{
    String itemName;
    boolean isAvaliable;
    Type type;
    boolean borrowable;
    

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isAvaliable() {
        return isAvaliable;
    }

    public void setAvaliable(boolean isAvaliable) {
        this.isAvaliable = isAvaliable;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isBorrowable() {
        return borrowable;
    }

    public void setBorrowable(boolean borrowable) {
        this.borrowable = borrowable;
    }

    public LibraryItem(String itemName, boolean isAvaliable, Type type) {
        this.itemName = itemName;
        this.isAvaliable = isAvaliable;
        this.type = type;

        if(type==Type.PERIODICAL){
            this.borrowable=false;
        }
        else this.borrowable=true;
    }

    
    
    
}


enum Type{
    BOOK (1),
    CD(2),
    DVD(3),
    PERIODICAL(4);

int typeCode;
private Type(int typeCode){
    this.typeCode=typeCode;
}
}