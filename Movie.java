public class Movie extends LibraryItem {
    String director;
    String runtime;
    Genre genre;
    
    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Movie(String itemName, boolean isAvaliable, Type type, String director, String runtime, Genre genre) {
        super(itemName, isAvaliable, type);
        this.director = director;
        this.runtime = runtime;
        this.genre = genre;
    }
    

}