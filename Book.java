public class Book extends LibraryItem {
    String author;
    int numPages;
    Genre genre;

    public Book(String itemName, boolean isAvaliable, Type type, String author, int numPages, Genre genre) {
        super(itemName, isAvaliable, type);
        this.author = author;
        this.numPages = numPages;
        this.genre = genre;
    }
    

}
