import java.util.*;
public class LibraryTester {
    public static void main(String[] args) {
        LibraryItem book1=new Book("The Guernsey Literary and Potato Peel Pie Society", true, Type.BOOK, "Mary Ann Shaffer", 320, Genre.ROMANCE);
        LibraryItem movie=new Movie("American Sniper", true, Type.DVD, "Clint Eastwood", "2hr, 12 min", Genre.ACTION);
        LibraryItem cd1= new CD("folklore", false, Type.CD, "Taylor Swift");
        LibraryItem p1= new Periodical("New York Times", true, Type.PERIODICAL, "US News");
        ArrayList<LibraryItem> myInventory= new ArrayList<LibraryItem>();
        Library myLibrary=new Library(myInventory);

        //Testing contents of library after adding each item type 
        System.out.println(myLibrary.toString()+"\n");
        myLibrary.addItem(book1);
        System.out.println(myLibrary.toString());
        myLibrary.addItem(movie);
        System.out.println(myLibrary.toString());
        myLibrary.addItem(cd1);
        System.out.println(myLibrary.toString());
        myLibrary.addItem(p1);
        System.out.println(myLibrary.toString());

        //Testing checking out a book
        Account gabbysAccount= new Account("gabbystillman", new ArrayList<LibraryItem> ());
        ArrayList<LibraryItem> gabbysItems= new ArrayList<LibraryItem>();
        gabbysItems.add(book1);
        gabbysAccount.checkout(gabbysItems);
        //Testing checking out a book that is unavaliable 
        Account kelvinsAccount=new Account("kferrei3", new ArrayList<LibraryItem>());
        ArrayList<LibraryItem> kelvinsItems=new ArrayList<LibraryItem>();
        kelvinsItems.add(book1);
        kelvinsItems.add(movie);
        kelvinsAccount.checkout(kelvinsItems);

        //Testing returning items
        gabbysAccount.returnItems(gabbysItems);

        //Now Kelvin should be able to check out guernsey
        kelvinsAccount.checkout(kelvinsItems);




    }
}